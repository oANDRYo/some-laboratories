﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	[Serializable]
	public class CodeNotFoundException : Exception
	{
		public CodeNotFoundException(bool[] code, int startIndex) : base(String.Format("Not found code {0} (first bit at index {1})", Utility.GetString(code), startIndex))
		{
		}
	}
}
