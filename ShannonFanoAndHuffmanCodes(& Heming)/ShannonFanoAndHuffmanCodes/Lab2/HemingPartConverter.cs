﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes.Lab2
{
	class HemingPartConverter
	{
		private ChecksumTable _checksumTable;
		private BitCountCalculator _bitCountCalculator;

		public ChecksumTable checksumTable
		{
			get
			{
				return _checksumTable;
			}
			set
			{
				_bitCountCalculator = value.bitsCount;

				_checksumTable = value;
			}
		}


		public HemingPartConverter(ChecksumTable checksumTable)
		{
			this.checksumTable = checksumTable;
		}

		private bool Add(bool a, bool b)
		{
			if ((!a && !b) || (a && b))
				return false;
			else return true;
		}

		public bool[] FromHemmingCode(bool[] hemmingCode)
		{
			int checksumCount = _bitCountCalculator.allBits - _bitCountCalculator.dataBits;
			bool[] checksum = new bool[checksumCount];

			for (int i = 0; i < checksumCount; i++)
			{
				bool sum = false;

				for (int j = 0; j < hemmingCode.Length; j++)
				{
					if (checksumTable.table[i, j])
					{
						sum = Add(sum, hemmingCode[j]);
					}
				}

				checksum[i] = sum;
			}

			for (int i = 0; i < hemmingCode.Length; i++)
			{
				bool equal = true;

				for (int j = 0; j < checksumCount; j++)
				{
					if (checksum[j] != checksumTable.table[j, i])
					{
						equal = false;

						break;
					}
				}

				if (equal)
				{
					hemmingCode[i] = !hemmingCode[i];
					break;
				}
			}

			bool[] normalCode = Utility.GetIncludeArray(hemmingCode, 0, _bitCountCalculator.dataBits - 1);

			return normalCode;
		}

		public bool[] ToHemmingCode(bool[] normalCode)
		{
			int checksumCount = _bitCountCalculator.allBits - _bitCountCalculator.dataBits;
			bool[] hemmingCode = new bool[_bitCountCalculator.allBits];

			for (int i = 0; i < normalCode.Length; i++)
			{
				hemmingCode[i] = normalCode[i];
			}

			for (int i = 0; i < checksumCount; i++)
			{
				bool sum = false;

				for (int j = 0; j < _bitCountCalculator.dataBits; j++)
				{
					if(_checksumTable.table[i,j])
					{
						sum = Add(sum, normalCode[j]);
					}
				}

				hemmingCode[_bitCountCalculator.dataBits + i] = sum;
			}

			return hemmingCode;
		}
	}
}
