﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes.Lab2
{
	class HemingConverter
	{
		private HemingPartConverter _hemmingPartConv;
		private ChecksumTable _checksumTable;

		public ChecksumTable checksumTable
		{
			get
			{
				return _checksumTable;
			}
			set
			{
				_hemmingPartConv.checksumTable = value;

				_checksumTable = value;
			}
		}

		public HemingConverter(ChecksumTable checksumTable)
		{
			_hemmingPartConv = new HemingPartConverter(checksumTable);

			_checksumTable = checksumTable;
		}

		public bool[] ToHemmingCode(bool[] normalCode)
		{
			int dataBits = _checksumTable.bitsCount.dataBits;
			int partsCount = normalCode.Length / dataBits;
			int hemmingCodeLenght = partsCount * _checksumTable.bitsCount.allBits;

			bool[] hemmingCode = new bool[hemmingCodeLenght];

			int k = 0;

			for (int i = 0; i < normalCode.Length; i += dataBits)
			{
				bool[] array = Utility.GetIncludeArray(normalCode, i, i + dataBits - 1);
				array = _hemmingPartConv.ToHemmingCode(array);

				for (int j = 0; j < array.Length; j++)
				{
					hemmingCode[k] = array[j];

					k++;
				}
			}

			return hemmingCode;
		}

		public bool[] FromHemmingCode(bool[] hemmingCode)
		{
			int partsCount = hemmingCode.Length / _checksumTable.bitsCount.allBits;

			bool[] normalCode = new bool[partsCount * _checksumTable.bitsCount.dataBits];

			int k = 0;

			for (int i = 0; i < hemmingCode.Length; i += _checksumTable.bitsCount.allBits)
			{
				bool[] part = Utility.GetIncludeArray(hemmingCode, i, i + _checksumTable.bitsCount.allBits - 1);

				part = _hemmingPartConv.FromHemmingCode(part);

				for (int j = 0; j < part.Length; j++)
				{
					normalCode[k] = part[j];

					k++;
				}
			}

			return normalCode;
		}
	}
}
