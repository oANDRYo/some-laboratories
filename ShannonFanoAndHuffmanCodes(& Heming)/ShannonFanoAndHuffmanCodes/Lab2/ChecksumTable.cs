﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes.Lab2
{
	class ChecksumTable
	{
		public BitCountCalculator bitsCount { get; set; }
		public bool[,] table { get; private set; }

		public ChecksumTable(BitCountCalculator bitsCount)
		{
			this.bitsCount = bitsCount;
		}

		private void SetVectorAtColumn(bool[] vector, int columnIndex)
		{
			for (int i = 0; i < vector.Length; i++)
			{
				table[i, columnIndex] = vector[i];
			}
		}

		private void FillServiceColumns()
		{
			for (int i = bitsCount.dataBits; i < table.GetLength(1); i++)
			{
				table[i - bitsCount.dataBits, i] = true;
			}
		}

		private bool IsTheLast(bool[] boolVector)
		{
			bool allTrue = true;

			foreach (var element in boolVector)
			{
				if (!element)
				{
					allTrue = false;
					return allTrue;
				}
			}

			return allTrue;
		}

		private bool IsAxis(bool[] array)
		{
			int count = 0;

			foreach (var item in array)
			{
				if (item)
				{
					count++;

					if (count == 2)
					{
						break;
					}
				}
			}

			return count == 1;
		}

		private bool[] GetNext(bool[] current)
		{
			for (int i = current.Length - 1; i >= 0; i--)
			{
				if (!current[i])
				{
					current[i] = true;

					for (int j = current.Length - 1; j > i; j--)
					{
						current[j] = false;
					}

					break;
				}
			}

			return current;
		}

		public bool[,] FillTable()
		{
			int serviceBitsCount = bitsCount.allBits - bitsCount.dataBits;
			table = new bool[serviceBitsCount, bitsCount.allBits];

			FillServiceColumns();

			bool[] vector = new bool[serviceBitsCount];

			vector = GetNext(vector);
			int i = bitsCount.dataBits - 1;

			while (true)
			{
				if (!IsAxis(vector))
				{
					SetVectorAtColumn(vector, i);

					i--;

					if (i < 0) break;
				}


				vector = GetNext(vector);
			}

			return table;
		}
	}
}
