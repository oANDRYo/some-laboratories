﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes.Lab2
{
	public struct BitCountCalculator
	{
		public int dataBits { get; private set; }
		public int allBits { get; private set; }

		public double errorProbability { get; private set; }

		public void Calculate(int serviceBits)
		{

			allBits = (int)Math.Pow(2, serviceBits) - 1 ;
			dataBits = allBits - serviceBits;

			errorProbability = 1.0 / allBits;
		}
	}
}
