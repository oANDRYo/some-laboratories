﻿using ShannonFanoAndHuffmanCodes.Lab2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShannonFanoAndHuffmanCodes
{
	public partial class Form1 : Form
	{
		private AlphabetCodeConnect _alphabetCodeConnect;
		private BitCountCalculator _bitCountCalc;
		private HemingConverter _hemingConverter;

		public Form1()
		{
			InitializeComponent();
			dataGridView1.RowCount = 1;
			dataGridView1.ColumnCount = 3;
			dataGridView1.Columns[0].HeaderCell.Value = "Символ";
			dataGridView1.Columns[1].HeaderCell.Value = "Код";
			dataGridView1.Columns[2].HeaderCell.Value = "Кількість повторень";
			dataGridView1.Columns[2].ReadOnly = true;

		}

		private void Form1_Load(object sender, EventArgs e)
		{
			numericUpDown2_ValueChanged(sender, e);
			richTextBox1_TextChanged(sender, e);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			IEncodeMethod method;

			if (radioButton1.Checked)
			{
				method = new ShannonFanoMethod(textBox1.Text);
			}
			else
			{
				method = new HuffmanMethod(textBox1.Text);
			}

			var result = method.Encode();

			textBox2.Text = Utility.GetString(result.encodedText);
			_alphabetCodeConnect = result.alphabetCodes;

			UpdateGridView(_alphabetCodeConnect, result.symbolCount);
		}

		private void UpdateGridView(AlphabetCodeConnect alphabet, Dictionary<char, uint> symbolCount)
		{
			numericUpDown1.Value = symbolCount.Count;

			for (int i = 0; i < symbolCount.Count; i++)
			{
				var symbol = symbolCount.ElementAt(i).Key;

				dataGridView1[0, i].Value = symbol;
				dataGridView1[1, i].Value = Utility.GetString(alphabet.connections[symbol]);
				dataGridView1[2, i].Value = symbolCount[symbol];
			}
		}

		private void numericUpDown1_ValueChanged(object sender, EventArgs e)
		{
			dataGridView1.RowCount = (int)numericUpDown1.Value;
		}

		private void button3_Click(object sender, EventArgs e)
		{
			var symbolCodes = new Dictionary<char, bool[]>();

			try
			{
				for (int i = 0; i < numericUpDown1.Value; i++)
				{
					char symbol = Convert.ToChar(dataGridView1[0, i].Value);
					bool[] code = Utility.GetCode(Convert.ToString(dataGridView1[1, i].Value));

					if (symbolCodes.ContainsKey(symbol))
					{
						throw new Exception();
					}
					else
					{
						symbolCodes.Add(symbol, code);
					}
				}

				_alphabetCodeConnect.connections = symbolCodes;

				MessageBox.Show("Успішно загружено");
			}
			catch
			{
				MessageBox.Show("Перевірте правильність введених даних!");
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			if (_alphabetCodeConnect == null)
			{
				MessageBox.Show("Алфавіт відсутній!");
				return;
			}
			try
			{
				DecodeMethod a = new DecodeMethod(Utility.GetCode(textBox2.Text), _alphabetCodeConnect);
				textBox1.Text = a.Decode();
			}
			catch (CodeNotFoundException ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void numericUpDown2_ValueChanged(object sender, EventArgs e)
		{
			int d = (int)numericUpDown2.Value;

			_bitCountCalc.Calculate(d);

			WriteAboutBits();
			
			richTextBox1_TextChanged(sender, e);
		}

		private void WriteAboutBits()
		{
			label3.Text = string.Format("Кількість інформаційних бітів {0}, всього бітів {1}, alpha {2}", _bitCountCalc.dataBits, _bitCountCalc.allBits, _bitCountCalc.errorProbability);
		}

		private void button4_Click(object sender, EventArgs e)
		{
			bool allStepMistake = checkBox1.Checked;

			char[] symbols = richTextBox1.Text.ToArray();

			if (symbols.Length % _bitCountCalc.allBits != 0)
			{
				MessageBox.Show("Кількість бітів не кратна " + _bitCountCalc.allBits);
				return;
			}

			Random rnd = new Random();

			for (int i = 0; i < symbols.Length; i += _bitCountCalc.allBits)
			{
				if (allStepMistake || rnd.Next(2) == 1)
				{
					int rndSymbolIndex = rnd.Next(i, i + _bitCountCalc.allBits);

					if (symbols[rndSymbolIndex] == '1')
					{
						symbols[rndSymbolIndex] = '0';
					}
					else
					{
						symbols[rndSymbolIndex] = '1';
					}
				}
			}

			string result = string.Empty;

			foreach (var symbol in symbols)
			{
				result += symbol;
			}

			richTextBox1.Text = result;
		}

		private void button6_Click(object sender, EventArgs e)
		{
			string text = richTextBox1.Text;

			if (text.Length % _bitCountCalc.allBits != 0)
			{
				MessageBox.Show("Кількість бітів не кратна " + _bitCountCalc.allBits);
				return;
			}

			bool[] hemmingCode = Utility.GetCode(text);

			ChecksumTable checksum = new ChecksumTable(_bitCountCalc);
			checksum.FillTable();

			_hemingConverter = new HemingConverter(checksum);

			bool[] normalCode = _hemingConverter.FromHemmingCode(hemmingCode);

			textBox2.Text = Utility.GetString(normalCode);

		}

		private void button5_Click(object sender, EventArgs e)
		{
			string text = textBox2.Text;

			if (text.Length % _bitCountCalc.dataBits != 0)
			{
				MessageBox.Show("Кількість бітів не кратна " + _bitCountCalc.dataBits);
				return;
			}

			bool[] normalCode = Utility.GetCode(text);

			ChecksumTable checksum = new ChecksumTable(_bitCountCalc);
			checksum.FillTable();

			_hemingConverter = new HemingConverter(checksum);

			bool[] hemingCode = _hemingConverter.ToHemmingCode(normalCode);

			richTextBox1.Text = Utility.GetString(hemingCode);
		}

		private void richTextBox1_TextChanged(object sender, EventArgs e)
		{
			int textLength = richTextBox1.TextLength;
			int partCount = textLength / _bitCountCalc.allBits;

			int sum = _bitCountCalc.dataBits;

			for (int i = 0; i < partCount + 1; i++)
			{
				richTextBox1.Select(i * _bitCountCalc.allBits, _bitCountCalc.allBits);

				if (i % 2 == 0)
					richTextBox1.SelectionBackColor = Color.LightGray;
				else
					richTextBox1.SelectionBackColor = Color.LightSlateGray;
			}

			richTextBox1.Select(richTextBox1.TextLength, 0);
		}
	}
}
