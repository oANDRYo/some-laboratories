﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	class ShannonFanoMethod : OrderedHistogramMethod
	{
		private Dictionary<char, uint> symbolsCount;

		public ShannonFanoMethod(string text) : base(text)
		{
		}

		public override EncodeMethodResult Encode()
		{
			symbolsCount = GetOrderedHistogram();

			int halfTextLength = text.Length / 2;

			var symbolsCodes = symbolsCount.ToDictionary(x => x.Key, x => new List<bool>());

			if (symbolsCodes.Count == 1)
			{
				symbolsCodes.ElementAt(0).Value.Add(true);
			}
			else
				Bisect(symbolsCodes);

			var codeAlphabetConnect = new AlphabetCodeConnect(symbolsCodes.ToDictionary(x => x.Key, x => x.Value.ToArray()));

			var result = new EncodeMethodResult(EncodeText(codeAlphabetConnect), codeAlphabetConnect, symbolsCount);

			return result;
		}

		private void Bisect(Dictionary<char, List<bool>> currentCodes)
		{
			if (currentCodes.Count < 2)
				return;

			uint symbolsCountAtCut = 0;

			foreach (var symbol in currentCodes.Keys)
			{
				symbolsCountAtCut += symbolsCount[symbol];
			}

			uint halfSymbolCount = symbolsCountAtCut / 2;
			uint currentCount = 0;
			int i = 0;

			var up = new Dictionary<char, List<bool>>();
			var down = new Dictionary<char, List<bool>>();

			for (; i < currentCodes.Count; i++)
			{
				var element = currentCodes.ElementAt(i);
				currentCount += symbolsCount[element.Key];

				up.Add(element.Key, element.Value);

				element.Value.Add(true);

				if (currentCount >= halfSymbolCount)
					break;
			}

			for (i++; i < currentCodes.Count; i++)
			{
				var element = currentCodes.ElementAt(i);

				down.Add(element.Key, element.Value);

				element.Value.Add(false);
			}

			Bisect(up);
			Bisect(down);
		}

	}
}
