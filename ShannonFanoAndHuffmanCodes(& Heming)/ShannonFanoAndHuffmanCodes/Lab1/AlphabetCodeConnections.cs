﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	public class AlphabetCodeConnect
	{
		public Dictionary<char, bool[]> connections { get; set; }

		public AlphabetCodeConnect(Dictionary<char, bool[]> connections)
		{
			this.connections = connections;
		}
	}
}
