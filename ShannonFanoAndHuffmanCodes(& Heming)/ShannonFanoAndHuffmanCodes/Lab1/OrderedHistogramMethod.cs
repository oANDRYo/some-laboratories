﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	public abstract class OrderedHistogramMethod : IEncodeMethod
	{
		public string text { get; set; }

		public OrderedHistogramMethod(string text)
		{
			this.text = text;
		}
		
		protected Dictionary<char, uint> GetOrderedHistogram()
		{
			var histogram = new Dictionary<char, uint>();

			foreach (var symbol in text)
			{
				if(histogram.ContainsKey(symbol))
				{
					histogram[symbol]++;
				}
				else
				{
					histogram.Add(symbol, 1);
				}
			}

			histogram = histogram.OrderByDescending(x => x.Value).ToDictionary(x=>x.Key,x=>x.Value);

			return histogram;
		}

		protected bool[] EncodeText(AlphabetCodeConnect alphabetCodeConnection)
		{
			var result = new List<bool>();

			var codeAlphabetConnect = alphabetCodeConnection.connections;

			foreach (var symbol in text)
			{
				foreach (var bit in codeAlphabetConnect[symbol])
				{
					result.Add(bit);
				} 
			}

			return result.ToArray();
		}


		public abstract EncodeMethodResult Encode();
	}
}
