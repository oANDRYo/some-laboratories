﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	public class EncodeMethodResult
	{
		public bool[] encodedText { get; private set; }
		public AlphabetCodeConnect alphabetCodes { get; private set; }
		public Dictionary<char, uint> symbolCount { get; private set; }

		public EncodeMethodResult(bool[] encodedText, AlphabetCodeConnect alphabetCodes, Dictionary<char, uint> symbolCount)
		{
			this.encodedText = encodedText;
			this.alphabetCodes = alphabetCodes;
			this.symbolCount = symbolCount;
		}
	}
}
