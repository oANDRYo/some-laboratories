﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	class Vertex
	{
		public uint value { get; set; }
		public bool isUp { get; set; }

		public Vertex right { get; set; }
		public Vertex up { get; set; }
		public Vertex down { get; set; }

		public Vertex(uint value)
		{
			this.value = value;
		}

	}
}
