﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	public class DecodeMethod
	{
		public bool[] encodedText { get; set; }
		public AlphabetCodeConnect alphabetCodes { get; set; }

		public DecodeMethod(bool[] encodedText, AlphabetCodeConnect alphabetCodes)
		{
			this.encodedText = encodedText;
			this.alphabetCodes = alphabetCodes;
		}

		public string Decode()
		{
			string message = string.Empty;
			List<bool> currentCode = new List<bool>();
			Dictionary<char, bool[]> correctPrefixSelection = Utility.GetCopy(alphabetCodes.connections);
			int checkedCodeLenght = 0;

			for (int bitNumber = 0; bitNumber < encodedText.Length; bitNumber++)
			{
				currentCode.Add(encodedText[bitNumber]);
				try
				{
					correctPrefixSelection = StartingWithPrefix(correctPrefixSelection, currentCode.ToArray(), checkedCodeLenght);
				}
				catch(Exception)
				{
					throw new CodeNotFoundException(currentCode.ToArray(), bitNumber - currentCode.Count + 1);
				}

				checkedCodeLenght++;

				if (correctPrefixSelection.Count == 1)
				{
					message += correctPrefixSelection.ElementAt(0).Key;

					checkedCodeLenght = 0;
					currentCode.Clear();
					correctPrefixSelection = Utility.GetCopy(alphabetCodes.connections);
				}
				else if (correctPrefixSelection.Count == 0)
				{
					throw new CodeNotFoundException(currentCode.ToArray(), bitNumber - currentCode.Count + 1);
				}
			}

			if (currentCode.Count != 0)
			{
				throw new CodeNotFoundException(currentCode.ToArray(), encodedText.Length - currentCode.Count);
			}

			return message;
		}

		private Dictionary<char, bool[]> StartingWithPrefix(Dictionary<char, bool[]> symbolCodes, bool[] prefix, int firstCheckedIndex = 0)
		{
			var selection = new Dictionary<char, bool[]>();

			for (int i = 0; i < symbolCodes.Count; i++)
			{
				var currentElement = symbolCodes.ElementAt(i);
				var elementCode = currentElement.Value;

				bool equalPrefix = true;

				for (int j = firstCheckedIndex; j < prefix.Length; j++)
				{
					if (elementCode[j] != prefix[j])
					{
						equalPrefix = false;
						break;
					}
				}

				if (equalPrefix)
				{
					selection.Add(currentElement.Key, currentElement.Value);
				}
			}

			return selection;
		}
	}
}
