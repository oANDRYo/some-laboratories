﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	class HuffmanMethod : OrderedHistogramMethod
	{
		public HuffmanMethod(string text) : base(text)
		{
		}

		public override EncodeMethodResult Encode()
		{
			var symbolCountDescend = GetOrderedHistogram();

			var symbolVertices = new Dictionary<char, Vertex>();
			var currentVerticles = new List<Vertex>();

			foreach (var symbol in symbolCountDescend)
			{
				var symbolVertex = new Vertex(symbol.Value);

				symbolVertices.Add(symbol.Key, symbolVertex);
				currentVerticles.Add(symbolVertex);
			}

			while (currentVerticles.Count != 1)
			{
				Vertex up = currentVerticles[currentVerticles.Count - 2];
				Vertex down = currentVerticles[currentVerticles.Count - 1];

				Vertex right = new Vertex(up.value + down.value);
				right.up = up;
				right.down = down;

				up.isUp = true;
				down.isUp = false;

				up.right = right;
				down.right = right;

				currentVerticles.Remove(up);
				currentVerticles.Remove(down);
				currentVerticles.Add(right);

				currentVerticles = currentVerticles.OrderByDescending(x => x.value).ToList();
			}

			var symbolCodes = new Dictionary<char, bool[]>();

			for (int i = 0; i < symbolVertices.Count; i++)
			{
				var symbolCode = new List<bool>();

				var element = symbolVertices.ElementAt(i);
				Vertex current = element.Value;

				symbolCode.Add(current.isUp);

				while (current.right != null && current.right.right != null)
				{
					current = current.right;

					symbolCode.Add(current.isUp);
				}

				symbolCode.Reverse();

				symbolCodes.Add(element.Key, symbolCode.ToArray());
			}

			var alphaBet = new AlphabetCodeConnect(symbolCodes);
			var methodResult = new EncodeMethodResult(EncodeText(alphaBet), alphaBet, symbolCountDescend);

			return methodResult;
		}
	}
}
