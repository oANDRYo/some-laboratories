﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	public interface IEncodeMethod
	{
		string text { get; set; }

		EncodeMethodResult Encode();
	}
}
