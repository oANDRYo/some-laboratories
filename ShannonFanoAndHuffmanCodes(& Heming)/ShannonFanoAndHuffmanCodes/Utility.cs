﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShannonFanoAndHuffmanCodes
{
	public static class Utility
	{
		public static Dictionary<char, bool[]> GetCopy(Dictionary<char, bool[]> original)
		{
			var copy = new Dictionary<char, bool[]>();

			foreach (var item in original)
			{
				copy.Add(item.Key, item.Value);
			}

			return copy;
		}

		public static string GetString(bool[] array)
		{
			string code = string.Empty;

			foreach (var bit in array)
			{
				code += (bit) ? 1 : 0;
			}

			return code;
		}

		public static bool[] GetCode(string code)
		{
			bool[] result = new bool[code.Length];

			for (int i = 0; i < code.Length; i++)
			{
				char symbol = code[i];
				if (symbol == '1')
				{
					result[i] = true;
				}
				else if (symbol == '0')
				{
					result[i] = false;
				}
				else
					throw new FormatException();
			}

			return result;
		}

		public static bool[] GetIncludeArray(bool[] array, int firstCopyIndex, int lastCopyIndex)
		{
			bool[] includeCopy = new bool[lastCopyIndex - firstCopyIndex + 1];

			for (int i = 0; i < includeCopy.Length; i++)
			{
				includeCopy[i] = array[firstCopyIndex + i];
			}

			return includeCopy;
		}
	}
}
