﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumericalIntegration
{
    class MediumRectangles : IntervalIntegral
    {
        public MediumRectangles(double a, double b, Function func, double epsilon)
            : base(a, b, func, epsilon)
        {
        }

        protected override double IntegralSearch()
        {
            double sum = 0; 

            for (double x = _a + _h/2; x < _b; x += _h)
            {
                sum += _function(x) * _h;
            }

            return sum;
        }
    }
}
