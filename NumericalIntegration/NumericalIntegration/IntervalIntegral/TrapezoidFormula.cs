﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumericalIntegration
{
    class TrapezoidFormula : IntervalIntegral
    {
        public TrapezoidFormula(double a, double b, Function func, double epsilon)
            : base(a, b, func, epsilon)
        {
        }

        protected override double IntegralSearch()
        {
            double sum = (_function(_a) + _function(_b)) / 2;

            for (double x = _a + _h; x < _b; x += _h)
            {
                sum += _function(x);
            }

            return sum * _h;
        }
    }
}
