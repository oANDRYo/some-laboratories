﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumericalIntegration
{
    class SimpsonFormula : IntervalIntegral
    {
        public SimpsonFormula(double a, double b, Function func, double epsilon)
            : base(a, b, func, epsilon)
        {
        }

        protected override double IntegralSearch()
        {
            double sum1 = 0;
            double sum2 = 0;

            for (double x = _a + _h; x < _b; x += _h)
            {
                sum1 += _function(x);
            }

            for (double x = _a + _h / 2; x < _b; x += _h)
            {
                sum2 += _function(x);
            }

            return _h * (_function(_a) + _function(_b) + 2 * sum1 + 4 * sum2) / 6;
        }
    }
}
