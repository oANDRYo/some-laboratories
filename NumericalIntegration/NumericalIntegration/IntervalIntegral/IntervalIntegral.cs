﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumericalIntegration
{
    abstract class IntervalIntegral
    {
        protected double _a;
        protected double _b;

        protected Function _function;

        protected double _epsilon;
        protected double _previousStepResult;

        protected uint _n = 1;
        protected double _h;

        private void UpdateStep()
        {
            _h = (_b - _a) / _n;
        }

        public IntervalIntegral(double a, double b, Function func, double epsilon)
        {
            _a = a;
            _b = b;
            _function = func;
            _epsilon = epsilon;
        }

        public void Initialize(double a,double b,Function func,double epsilon)
        {
            _a = a;
            _b = b;
            _function = func;
            _epsilon = epsilon;
        }

        public double Calculate()
        {
            UpdateStep();
            double currentStepResult = IntegralSearch();

            do
            {
                _previousStepResult = currentStepResult;
                _n *= 2;

                UpdateStep();

                currentStepResult = IntegralSearch();
            }
            while (Math.Abs(currentStepResult - _previousStepResult) > _epsilon);

            return currentStepResult;
        }

        protected abstract double IntegralSearch();

    }
    
}
