﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumericalIntegration
{
    interface IntervalIntegralFactory
    {
         IntervalIntegral CreateIntervalIntegral(double a, double b, Function func, double epsilon);
    }

    class LeftRectanglesFactory : IntervalIntegralFactory
    {
        public IntervalIntegral CreateIntervalIntegral(double a, double b, Function func, double epsilon)
        {
            return new LeftRectangles(a, b, func, epsilon);
        }
    }

    class RightRectanglesFactory : IntervalIntegralFactory
    {
        public IntervalIntegral CreateIntervalIntegral(double a, double b, Function func, double epsilon)
        {
            return new RightRectangles(a, b, func, epsilon);
        }
    }

    class MediumRectanglesFactory : IntervalIntegralFactory
    {
        public IntervalIntegral CreateIntervalIntegral(double a, double b, Function func, double epsilon)
        {
            return new MediumRectangles(a, b, func, epsilon);
        }
    }

    class TrapezoidFormulaFactory : IntervalIntegralFactory
    {
        public IntervalIntegral CreateIntervalIntegral(double a, double b, Function func, double epsilon)
        {
            return new TrapezoidFormula(a, b, func, epsilon);
        }
    }

    class SimpsonFormulaFactory : IntervalIntegralFactory
    {
        public IntervalIntegral CreateIntervalIntegral(double a, double b, Function func, double epsilon)
        {
            return new SimpsonFormula(a, b, func, epsilon);
        }
    }
}
