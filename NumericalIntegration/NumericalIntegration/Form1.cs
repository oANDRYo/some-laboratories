﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NumericalIntegration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double a, b, epsilon;

            if (GetCorrectTextBoxValues(out a, out b, out epsilon))
            {
                IntervalIntegralFactory factory = GetSelectedFactory();

                IntegrationAlgorithm integralAlgorithm = new IntegrationAlgorithm(a, b, 
                    (uint)intervalsCount.Value, x =>x*x, epsilon, factory);

                /* Math.Pow(Math.Log(x), 2)*/

                MessageBox.Show(integralAlgorithm.Calculate().ToString());
            }
            else
            {
                MessageBox.Show("Incorrect a/b/epsilon value");
            }
            
        }

        private IntervalIntegralFactory GetSelectedFactory()
        {
            IntervalIntegralFactory factory;

            if (radioButton1.Checked)
            {
                factory = new LeftRectanglesFactory();
            }
            else if (radioButton2.Checked)
            {
                factory = new RightRectanglesFactory();
            }
            else if (radioButton3.Checked)
            {
                factory = new MediumRectanglesFactory();
            }
            else if (radioButton4.Checked)
            {
                factory = new TrapezoidFormulaFactory();
            }
            else
            {
                factory = new SimpsonFormulaFactory();
            }

            return factory;
        }

        private bool GetCorrectTextBoxValues(out double a, out double b, out double epsilon)
        {
            try
            {
                a = Convert.ToDouble(aTextBox.Text);
                b = Convert.ToDouble(bTextBox.Text);
                epsilon = Convert.ToDouble(epsilonTextBox.Text);

                if (a > b)
                {
                    return false;
                }

                return true;
            }
            catch
            {
                a = 0;
                b = 0;
                epsilon = 0;

                return false;
            }
        }
    }
}
