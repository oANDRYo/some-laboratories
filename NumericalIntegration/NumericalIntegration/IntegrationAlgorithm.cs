﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumericalIntegration
{
    delegate double Function(double x);

    class IntegrationAlgorithm
    {
        double A
        {
            get;
            set;
        }
        double B
        {
            get;
            set;
        }

        Function Function1
        {
            get;
            set;
        }

        double Epsilon
        {
            get;
            set;
        }

        uint IntervalsCount
        {
            get;
            set;
        }

        IntervalIntegralFactory IntervalFactory
        {
            get;
            set;
        }

        public IntegrationAlgorithm(double a, double b, uint intervalCount,
            Function function, double epsilon, IntervalIntegralFactory factory)
        {
            A = a;
            B = b;
            IntervalsCount = intervalCount;
            Function1 = function;
            Epsilon = epsilon;
            IntervalFactory = factory;
        }

        public double Calculate()
        {
            double sum = 0;
            double h = (B - A) / IntervalsCount;

            for (int i=0; i < IntervalsCount; i++)
            {
                IntervalIntegral interval = IntervalFactory.CreateIntervalIntegral(A+h*i, A+h*(i+1), Function1, Epsilon);
                sum += interval.Calculate();
            }

            return sum;
        }
    }
}
